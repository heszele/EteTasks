package com.rudolfheszele.etetasks

import net.fortuna.ical4j.model.Date
import net.fortuna.ical4j.model.DateTime
import java.util.*

/**
 * The sole purpose of this class is to be able to compare TimeDate objects
 * in different timezones
 * To achieve this, TimeDate objects are converted to UTC
 * and then compared
 */
class TaskDate(date: Date?) {
    val date: Date?
        get() = mDate

    val utcDateTime: DateTime?
        get() = mUtcDateTime

    override fun equals(other: Any?): Boolean {
        if(this === other) {
            return true
        }
        if(other === null || javaClass !== other.javaClass) {
            return false
        }

        val taskDate = other as TaskDate

        // Either both or neither should have a UTC DateTime
        if((null != mUtcDateTime) xor (null != taskDate.mUtcDateTime)) {
            return false
        }

        // If there is UTC DateTime, use that one for comparison
        if(null != mUtcDateTime) {
            return mUtcDateTime == taskDate.mUtcDateTime
        }

        // Otherwise just compare the Dates
        return mDate == taskDate.mDate
    }

    override fun hashCode(): Int {
        return Objects.hash(mDate)
    }

    private val mDate: Date? = date
    private var mUtcDateTime: DateTime? = null

    init {
        if(mDate is DateTime) {
            mUtcDateTime = mDate.clone() as DateTime
            mUtcDateTime!!.isUtc = true
        }
    }
}
