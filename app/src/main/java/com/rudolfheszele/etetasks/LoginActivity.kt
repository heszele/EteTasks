package com.rudolfheszele.etetasks

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.CheckBox
import android.widget.EditText

const val LOGIN_TAG = "com.rudolfheszele.etetasks.LOGIN"
const val SUCCESS_TAG = "com.rudolfheszele.etetasks.SUCCESS"
const val USERNAME_TAG = "com.rudolfheszele.etetasks.USERNAME"
const val PASSWORD_TAG = "com.rudolfheszele.etetasks.PASSWORD"
const val SAVE_SESSION_TAG = "com.rudolfheszele.etetasks.SAVE_SESSION"

class LoginActivity : ThemedActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
    }

    fun onLogin(view: View) {
        val intent = Intent(this, TaskListActivity::class.java)
        val username: String = findViewById<EditText>(R.id.usernamePlainText).text.toString()
        val password: String = findViewById<EditText>(R.id.passwordPassword).text.toString()
        val saveSession: Boolean = findViewById<CheckBox>(R.id.saveSessionCheckBox).isChecked

        // This Activity should never be part of the Activity stack,
        // but setting noHistory on the layout XML does not seem to work
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        intent.putExtra(LOGIN_TAG, true)
        intent.putExtra(SUCCESS_TAG, true)
        intent.putExtra(USERNAME_TAG, username)
        intent.putExtra(PASSWORD_TAG, password)
        intent.putExtra(SAVE_SESSION_TAG, saveSession)

        startActivity(intent)
    }
}