package com.rudolfheszele.etetasks;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;

import com.google.android.material.chip.Chip;
import com.rudolfheszele.etetasks.etebase.EtebaseCollection;

import net.fortuna.ical4j.model.Date;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

// TODO: Show start date as well

class TaskDetailsView extends ConstraintLayout {

    public TaskDetailsView(Context context) {
        super(context);
        LayoutInflater.from(context).inflate(R.layout.task_details_view, this);
        setup();
    }

    public TaskDetailsView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.task_details_view, this);
        setup();
    }

    public TaskDetailsView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        LayoutInflater.from(context).inflate(R.layout.task_details_view, this);
        setup();
    }

    public TaskDetailsView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        LayoutInflater.from(context).inflate(R.layout.task_details_view, this);
        setup();
    }

    public void setTask(SubTask task, EtebaseCollection collection) {
        mTask = task;

        mSummaryTextView.setText(null != mTask.getSummary() ? mTask.getSummary() : "");
        if(null == mTask.getDescription() || mTask.getDescription().isEmpty()) {
            mDescriptionTextView.setVisibility(View.GONE);
        }
        else {
            mDescriptionTextView.setText(mTask.getDescription());
        }
        setDueDate();
        // TODO: Instead of hardcoded colours, it would be better to have some themed colours for these
        if(mTask.isOverdue()) {
            mDueDateTextView.setTextColor(getContext().getResources().getColor(R.color.red_300, null));
        }
        else if(mTask.isOngoing()) {
            mDueDateTextView.setTextColor(getContext().getResources().getColor(R.color.green_300, null));
        }
        if(mTask.isCompleted()) {
            mCompletedCheckBox.setChecked(true);
            mSummaryTextView.setPaintFlags(mSummaryTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            mDescriptionTextView.setPaintFlags(mDescriptionTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            mDueDateTextView.setPaintFlags(mDueDateTextView.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
        else {
            mCompletedCheckBox.setChecked(false);
        }
        mRecurringImageView.setVisibility(mTask.isRecurring() ? View.VISIBLE : View.GONE);
        if(null != collection) {
            mCollectionChip.setText(collection.getName());
        }
        else {
            mCollectionChip.setVisibility(View.GONE);
        }
    }

    public void indent() {
        ConstraintSet constraintSet = new ConstraintSet();
        ConstraintLayout constraintLayout = findViewById(R.id.taskDetailsContraintLayout);
        int indent = getContext().getResources().getDimensionPixelSize(R.dimen.recurring_item_indent);

        constraintSet.clone(constraintLayout);
        constraintSet.setMargin(mCompletedCheckBox.getId(), ConstraintSet.START, indent);
        constraintSet.applyTo(constraintLayout);
    }

    private void setup() {
        mCompletedCheckBox = findViewById(R.id.taskCompletedCheckBox);
        mSummaryTextView = findViewById(R.id.taskSummaryTextView);
        mDueDateTextView = findViewById(R.id.taskDueDateTextView);
        mDescriptionTextView = findViewById(R.id.taskDescriptionTextView);
        mRecurringImageView = findViewById(R.id.taskRecurringImageView);
        mCollectionChip = findViewById(R.id.taskCollectionChip);
    }

    private void setDueDate() {
        mDueDateTextView.setText("");

        Date due = mTask.getNextEnd();

        if(null == due) {
            return;
        }

        final DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.SHORT);
        mDueDateTextView.setText(formatter.format(due));
        // SimpleDateFormat formatter = new SimpleDateFormat("EEE, MMM dd, yyyy");
        //mDueDateTextView.setText(datesList.get(0).toString());
    }

    private CheckBox mCompletedCheckBox = null;
    private TextView mSummaryTextView = null;
    private TextView mDueDateTextView = null;
    private TextView mDescriptionTextView = null;
    private ImageView mRecurringImageView = null;
    private Chip mCollectionChip = null;
    private SubTask mTask = null;
}

public class TaskView extends LinearLayout implements View.OnLongClickListener {

    public TaskView(Context context) {
        super(context);
        create();
    }

    public TaskView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        create();
    }

    public TaskView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        create();
    }

    public TaskView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        create();
    }

    public void setTask(Task task, EtebaseCollection collection) {
        mTask = task;

        removeAllViews();

        mMainTaskDetails = new TaskDetailsView(getContext());
        mMainTaskDetails.setTask(task.getMainTask(), collection);

        mMainTaskDetails.setClickable(false);
        mMainTaskDetails.setLongClickable(true);
        mMainTaskDetails.setOnLongClickListener(this);

        // TODO: Somehow it should be visible on the main task
        //       if there are subtasks
        for(SubTask subTask: task.getSubTasks()) {
            TaskDetailsView details = new TaskDetailsView(getContext());

            details.setTask(subTask, collection);
            details.setVisibility(View.GONE);
            details.indent();
            addView(details);
            mSubTaskDetails.add(details);
        }
        addView(mMainTaskDetails);
    }

    private void create() {
        setOrientation(LinearLayout.VERTICAL);
    }

    @Override
    public boolean onLongClick(View view) {
        for(View subTask: mSubTaskDetails) {
            if(subTask.getVisibility() == View.GONE) {
                subTask.setVisibility(View.VISIBLE);
            }
            else if(subTask.getVisibility() == View.VISIBLE) {
                subTask.setVisibility(View.GONE);
            }
        }

        return false;
    }

    private Task mTask = null;
    private TaskDetailsView mMainTaskDetails = null;
    private final List<TaskDetailsView> mSubTaskDetails = new ArrayList<>();
}