package com.rudolfheszele.etetasks

import net.fortuna.ical4j.model.*
import net.fortuna.ical4j.model.Calendar
import net.fortuna.ical4j.model.Date
import net.fortuna.ical4j.model.component.VToDo
import net.fortuna.ical4j.model.property.*
import java.time.temporal.TemporalAmount

class SubTask() {
    constructor(task: VToDo, mainTask: Task): this() {
        mTask = task
        mMainTask = mainTask
    }

    val uid: String
        get() = mTask.getProperty<Uid>(Property.UID).value

    val summary: String?
        get() = mTask.getProperty<Summary>(Property.SUMMARY)?.value

    val description: String?
        get() = mTask.getProperty<Description>(Property.DESCRIPTION)?.value

    val isRecurring: Boolean
        get() {
            return null != mTask.getProperty<RRule>(Property.RRULE) ||
                   0 != mTask.getProperties<RDate>(Property.RDATE).size
        }

    val isCompleted: Boolean
        get() = mTask.getProperty<Status>(Property.STATUS) == Status.VTODO_COMPLETED

    val lastModified: DateTime
        get() = mTask.getProperty<LastModified>(Property.LAST_MODIFIED).dateTime

    val start: Date?
        get() {
            mTask.getProperty<DtStart>(Property.DTSTART)?.let {
                return it.date
            }

            return null
        }

    val end: Date?
        get() {
            mTask.getProperty<DtEnd>(Property.DTEND)?.let {
                return it.date
            }
            mTask.getProperty<Due>(Property.DUE)?.let {
                return it.date
            }

            return null
        }

    val duration: TemporalAmount?
        get() {
            mTask.getProperty<Duration>(Property.DURATION)?.let {
                return it.duration
            }

            start ?: return java.time.Duration.ZERO
            end ?: return java.time.Duration.ZERO

            return TemporalAmountAdapter.fromDateRange(start, end).duration
        }

    val nextStart: Date?
        get() {
            start ?: return null

            // Possible dates
            val dates: MutableSet<TaskDate> = mutableSetOf()

            // start is a possible date
            dates.add(TaskDate(start!!))

            // Get possible dates from RDATE properties
            val rDates: List<TaskDate> = mTask.getProperties<RDate>(Property.RDATE).flatMap {
                it.dates
            }.map {
                TaskDate(it)
            }
            dates.addAll(rDates)

            // Remove dates from EXDATE properties
            val exDates: List<TaskDate> = mTask.getProperties<ExDate>(Property.EXDATE).flatMap {
                it.dates
            }.map {
                TaskDate(it)
            }
            dates.removeAll(exDates)

            // Add all completed recurrences' start date
            dates.removeAll(mMainTask!!.getCompletedStartDates(this).map { TaskDate(it) })

            dates.removeIf { null == it.date }
            // If we still have possible dates, just return the first one
            if(dates.isNotEmpty()) {

                return dates.minByOrNull { it.date!! }!!.date
            }

            // All dates were excluded, try to generate a valid one from RRULE
            val rRule: RRule = mTask.getProperty(Property.RRULE) ?: return null
            val recur = rRule.recur
            var next = recur.getNextDate(start, start)

            while(null != next) {
                if(!exDates.contains(TaskDate(next))) {
                    return next
                }
                next = recur.getNextDate(start, next)
            }

            return null
        }

    val nextEnd: Date?
        get() {
            // If no start date. must be a non-recurring item, so just return end
            val start = nextStart ?: return end

            val duration = duration ?: return null

            if(start is DateTime) {
                return DateTime(Date.from(start.toInstant().plus(duration)), start.timeZone)
            }

            return Date(Date.from(start.toInstant().plus(duration)))
        }

    val isOverdue: Boolean
        get() {
            val nextEnd = nextEnd ?: return false

            return nextEnd.before(DateTime())
        }

    val isOngoing: Boolean
        get() {
            val nextStart = nextStart ?: return false
            val now = DateTime()

            val nextEnd = nextEnd ?: return nextStart.before(now)

            return nextStart.before(now) && nextEnd.after(now)
        }

    private var mTask: VToDo = VToDo()
    private var mMainTask: Task? = null
}

class Task(calendar: Calendar) {
    val calendar: Calendar
        get() = mCalendar

    val mainTask: SubTask
        get() = mMainTask

    val subTasks: List<SubTask>
        get() = mSubTasks

    fun getCompletedStartDates(subTask: SubTask): List<Date> {
        val result: MutableList<Date> = mutableListOf()

        if(subTask == mMainTask) {
            result.addAll(mSubTasks.filter { it.nextStart != null }.map { it.nextStart!! })
        }

        return result
    }

    private val mCalendar: Calendar = calendar
    private var mMainTask: SubTask = SubTask()
    private val mSubTasks: MutableList<SubTask> = mutableListOf()

    init {
        val tasks = calendar.getComponents<VToDo>(Component.VTODO)

        if(tasks.isEmpty()) {
            throw Exception("No VTODO components found in calendar!")
        }

        if(1 == tasks.size) {
            // Simple task with only one VTODO component
            mMainTask = SubTask(tasks[0], this)
        }
        else {
            // There are more VTODO components
            // Most probably a recurring task, with completed occurrences
            // TODO: What if there are RDATE properties?
            // TODO: How do subtasks look like?
            val mainTasks = tasks.filter { it -> null != it.getProperty<RRule>(Property.RRULE) }
            val subTasks = tasks.filter { it -> null == it.getProperty<RRule>(Property.RRULE) }

            // Some sanity check
            if(mainTasks.count() != 1) {
                throw Exception("There can only be one recurring VTODO component!")
            }
            if(mainTasks.count() + subTasks.count() != tasks.size) {
                throw Exception("Internal error!")
            }

            mMainTask = SubTask(mainTasks.first(), this)
            for(subTask in subTasks) {
                mSubTasks.add(SubTask(subTask, this))
            }
        }
    }
}