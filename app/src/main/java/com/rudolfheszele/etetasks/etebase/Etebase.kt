package com.rudolfheszele.etetasks.etebase

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.etebase.client.Account
import com.etebase.client.Client
import com.etebase.client.FetchOptions
import com.etebase.client.exceptions.ConnectionException
import com.etebase.client.exceptions.UnauthorizedException
import com.rudolfheszele.etetasks.TAG
import com.rudolfheszele.etetasks.TaskView
import com.rudolfheszele.etetasks.json.JsonReader
import com.rudolfheszele.etetasks.json.JsonWriter
import okhttp3.OkHttpClient
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.io.OutputStream
import javax.json.Json

class Etebase: ViewModel() {
    val collections: List<EtebaseCollection>
        get() = mData!!.collections

    fun login(username: String, password: String): Result<Boolean> {
        val client = mClient ?: return Result.failure(Exception("No client to login with!"))
        try {
            mAccount = Account.login(client, username, password)
        }
        catch (connectionException: ConnectionException) {
            return Result.failure(connectionException)
        }
        catch(unauthorizedException: UnauthorizedException) {
            return Result.failure(unauthorizedException)
        }

        return Result.success(true)
    }

    fun logout() {
        val account = mAccount ?: return

        account.logout()
        mAccount = null
        mData = EtebaseData()
    }

    fun isLoggedIn(): Boolean {
        return null != mAccount
    }

    fun hasData(): Boolean {
        return null != mData
    }

    fun storeSession(output: OutputStream): Result<Boolean> {
        val account = mAccount ?: return Result.failure(Exception("No session to store!"))
        val session = account.save(null)

        with(output) {
            write(session.toByteArray(Charsets.UTF_8))
            flush()
            close()
        }

        return Result.success(true)
    }

    fun restoreSession(input: InputStream): Result<Boolean> {
        val client = mClient ?: return Result.failure(Exception("No client to restore session on!"))
        val session = String(input.readBytes(), Charsets.UTF_8)

        mAccount = Account.restore(client, session, null)

        return Result.success(true)
    }

    fun serialize(output: OutputStream): Result<Boolean> {
        val data = mData

        if(null == data || !data.isReady.get()) {
            return Result.failure(Exception("Data is not ready to be serialized"))
        }

        val generator = Json.createGenerator(output)
        val writer = JsonWriter(generator)

        data.serialize(writer)
        writer.close()

        return Result.success(true)
    }

    fun deserialize(input: InputStream) {
        val json = ByteArrayInputStream(input.readBytes())
        val parser = Json.createParser(json)
        val reader = JsonReader(parser)
        val data = EtebaseData(reader)

        data.checkIntegrity()
        data.isReady.set(true)
        mData = data
    }

    fun sync(): Result<Boolean> {
        val account = mAccount ?: return Result.failure(Exception("No account to sync with"))
        val collectionManager = account.collectionManager
        val data: EtebaseData = if(null != mData) mData!! else EtebaseData()
        var collectionsSyncToken: String? = data.syncToken
        var collectionsDone = false

        while(!collectionsDone) {
            // TODO: This can throw a ConnectionException - and I guess everything which talks
            //       to the server in the back.
            val etebaseCollections = collectionManager.list("etebase.vtodo", FetchOptions().stoken(collectionsSyncToken))

            collectionsDone = etebaseCollections.isDone
            collectionsSyncToken = etebaseCollections.stoken

            for ((collectionCounter, collection) in etebaseCollections.data.withIndex()) {
                Log.i(TAG, "Fetching collection '${collection.meta.name}' (${collectionCounter}/${etebaseCollections.data.size})")
                val existingCollectionIndex = data.collections.indexOfFirst {
                    it.uid == collection.uid
                }
                val isExistingCollection = existingCollectionIndex != -1

                if(collection.isDeleted) {
                    if(isExistingCollection) {
                        data.collections.removeAt(existingCollectionIndex)
                    }
                    continue
                }

                val existingCollection = if(isExistingCollection) data.collections[existingCollectionIndex] else null
                val itemManager = collectionManager.getItemManager(collection)
                var itemsSyncToken = existingCollection?.syncToken
                var itemsDone = false
                val etebaseCollection = if(isExistingCollection) existingCollection!! else EtebaseCollection(collection)

                while (!itemsDone) {
                    val etebaseItems = itemManager.list(FetchOptions().stoken(itemsSyncToken))

                    itemsDone = etebaseItems.isDone
                    itemsSyncToken = etebaseItems.stoken

                    for ((itemCounter, item) in etebaseItems.data.withIndex()) {
                        Log.i(TAG, "Fetching item '${item.meta.name}' (${itemCounter}/${etebaseItems.data.size})")
                        val existingItemIndex = etebaseCollection.items.indexOfFirst {
                            it.uid == item.uid
                        }
                        val isExistingItem = existingItemIndex != -1
                        if (item.isDeleted) {
                            // There can be cases when etebase reports the same task (with the same UID) twice
                            // Once deleted and once not.
                            // So if we see a deleted item, we check if the item contains an already added task or not
                            // and if yes, we remove it
                            // TODO: What if the deleted item comes first, and then the non-deleted one?
                            etebaseCollection.items.removeIf {
                                    etebaseItem -> etebaseItem.task.mainTask.uid == EtebaseItem(item, etebaseCollection).task.mainTask.uid
                            }
                            if(isExistingItem) {
                                etebaseCollection.items.removeAt(existingItemIndex)
                            }
                            continue
                        }

                        // Remove old item
                        if(isExistingItem) {
                            etebaseCollection.items.removeAt(existingItemIndex)
                        }

                        etebaseCollection.items.add(EtebaseItem(item, etebaseCollection))
                    }
                }
                etebaseCollection.syncToken = itemsSyncToken!!
                if(!isExistingCollection) {
                    data.collections.add(etebaseCollection)
                }
            }
        }
        data.syncToken = collectionsSyncToken!!
        data.checkIntegrity()
        data.isReady.set(true)

        mData = data

        return Result.success(true)

    }

    fun reSync(): Result<Boolean> {
        mData?.clear()

        return sync()
    }

    private var mClient: Client? = null
    private var mAccount: Account? = null
    private var mData: EtebaseData? = null

    init {
        val httpClient = OkHttpClient.Builder().build()
        mClient = Client.create(httpClient, null)
    }
}
