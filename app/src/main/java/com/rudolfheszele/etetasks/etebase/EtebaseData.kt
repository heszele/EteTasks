package com.rudolfheszele.etetasks.etebase

import com.etebase.client.Collection
import com.etebase.client.Item
import com.rudolfheszele.etetasks.Task
import com.rudolfheszele.etetasks.json.JsonReader
import com.rudolfheszele.etetasks.json.JsonWriter
import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.model.Calendar
import java.io.StringReader
import java.util.concurrent.atomic.AtomicBoolean

class EtebaseItem() {
    constructor(item: Item, collection: EtebaseCollection): this() {
        mUid = item.uid
        mCalendar = CalendarBuilder().build(StringReader(item.contentString))
        mTask = Task(mCalendar!!)
        mCollection = collection
    }

    constructor(reader: JsonReader, collection: EtebaseCollection): this() {
        reader.beginObject()
        mCollection = collection
        while(reader.hasNext()) {
            when(reader.nextName()) {
                "content" -> {
                    mCalendar = CalendarBuilder().build(StringReader(reader.nextString()))
                    mTask = Task(mCalendar!!)
                }
                "uid" -> {
                    mUid = reader.nextString()
                }
            }
        }
        reader.endObject()
    }

    val uid: String
        get() = mUid

    val task: Task
        get() = mTask!!

    var userData: Any?
        get() = mUserData
        set(value) {
            mUserData = value
        }

    val collection: EtebaseCollection
        get() = mCollection!!

    fun serialize(writer: JsonWriter) {
        writer.beginObject()
        writer.name("content")
        writer.value(mCalendar.toString())
        writer.name("uid")
        writer.value(mUid)
        writer.endObject()
    }

    private var mUid: String = String()
    private var mCalendar: Calendar? = null
    private var mTask: Task? = null
    private var mUserData: Any? = null
    private var mCollection: EtebaseCollection? = null
}

class EtebaseCollection() {
    constructor(collection: Collection): this() {
        mUid = collection.uid
        mName = collection.meta.name ?: return
    }

    constructor(reader: JsonReader): this() {
        reader.beginObject()
        while(reader.hasNext()) {
            when(reader.nextName()) {
                "stoken" -> {
                    mSyncToken = reader.nextString()
                }
                "uid" -> {
                    mUid = reader.nextString()
                }
                "name" -> {
                    mName = reader.nextString()
                }
                "items" -> {
                    reader.beginArray()
                    while(reader.hasNext()) {
                        mItems.add(EtebaseItem(reader, this))
                    }
                    reader.endArray()
                }
            }
        }
        reader.endObject()
    }

    var syncToken: String
        get() = mSyncToken
        set(value) {
            mSyncToken = value
        }

    val items: MutableList<EtebaseItem>
        get() = mItems

    var userData: Any?
        get() = mUserData
        set(value) {
            mUserData = value
        }

    val uid: String
        get() = mUid

    val name: String
        get() = mName

    fun serialize(writer: JsonWriter) {
        writer.beginObject()
        writer.name("stoken")
        writer.value(mSyncToken)
        writer.name("uid")
        writer.value(mUid)
        writer.name("name")
        writer.value(mName)
        writer.name("items")
        writer.beginArray()
        for(item in mItems) {
            item.serialize(writer)
        }
        writer.endArray()
        writer.endObject()
    }

    private var mUid: String = String()
    private var mName: String = String()
    private var mSyncToken: String = String()
    private val mItems: MutableList<EtebaseItem> = mutableListOf()
    private var mUserData: Any? = null
}

class EtebaseData() {
    constructor(reader: JsonReader): this() {
        reader.beginObject()
        while(reader.hasNext()) {
            when(reader.nextName()) {
                "stoken" -> {
                    mSyncToken = reader.nextString()
                }
                "collections" -> {
                    reader.beginArray()
                    while(reader.hasNext()) {
                        mCollections.add(EtebaseCollection(reader))
                    }
                    reader.endArray()
                }
            }
        }
        reader.endObject()
    }

    var syncToken: String?
        get() = mSyncToken
        set(value) {
            mSyncToken = value
        }

    val isReady: AtomicBoolean
        get() = mIsReady

    val collections: MutableList<EtebaseCollection>
        get() = mCollections

    fun clear() {
        mSyncToken = null
        mCollections.clear()
        mIsReady.set(false)
    }

    fun serialize(writer: JsonWriter) {
        writer.beginObject()
        writer.name("stoken")
        // TODO: What if mSyncToken is null?
        writer.value(mSyncToken!!)
        writer.name("collections")
        writer.beginArray()
        for(collection in mCollections) {
            collection.serialize(writer)
        }
        writer.endArray()
        writer.endObject()
    }

    fun checkIntegrity() {
        for(collection in mCollections) {
            val uids = mutableSetOf<Pair<String, EtebaseItem>>()
            val itemsToRemove = mutableSetOf<EtebaseItem>()
            for(item in collection.items) {
                val uid = item.task.mainTask.uid
                val currentPair = Pair(uid, item)

                val existingPair = uids.find { pair -> pair.first == uid }
                if(null != existingPair) {
                    // We have two items with the same UID - one is not needed
                    val existingLastModifiedDate = existingPair.second.task.mainTask.lastModified
                    val currentLastModifiedDate = item.task.mainTask.lastModified

                    if(currentLastModifiedDate.after(existingLastModifiedDate)) {
                        // The currently processed item is newer than the existing one
                        // We remove the old one
                        itemsToRemove.add(existingPair.second)
                    }
                    // else we do not need the current pair, so do not add it
                }
                else {
                    uids.add(currentPair)
                }
            }
            // Remove items
            for(item in itemsToRemove) {
                collection.items.remove(item)
            }
        }
    }

    private var mSyncToken: String? = null
    private val mCollections : MutableList<EtebaseCollection> = mutableListOf()
    private val mIsReady: AtomicBoolean = AtomicBoolean(false)
}