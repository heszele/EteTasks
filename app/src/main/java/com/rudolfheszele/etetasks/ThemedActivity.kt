package com.rudolfheszele.etetasks

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.security.crypto.EncryptedFile
import androidx.security.crypto.MasterKey
import com.rudolfheszele.etetasks.json.JsonReader
import com.rudolfheszele.etetasks.json.JsonWriter
import java.io.File
import javax.json.Json

class UiState: ViewModel() {
    enum class Order {
        DUE_DATE,
        SUMMARY
    }

    enum class SmartList {
        ALL_TASKS,
        TODAY,
        NEXT_WEEK,
        OVERDUE_TASKS,
        COMPLETED_TASKS,
        NONE
    }

    enum class Theme {
        GRAY,
        YELLOW,
        BLUE
    }

    data class TaskList(var name: String = "", var uid: String = "")

    var order: Order
        get() = mOrder
        set(value) {
            mOrder = value
        }

    var theme: Theme
        get() = mTheme
        set(value) {
            mTheme = value
        }

    var isShowCompletedItems: Boolean
        get() = mShowCompletedItems
        set(value) {
            mShowCompletedItems = value
        }

    val isSmartListSet: Boolean
        get() = mSmartList != SmartList.NONE

    fun clearSmartList() {
        mSmartList = SmartList.NONE
    }

    var smartList: SmartList
        get() = mSmartList
        set(value) {
            mSmartList = value
        }

    val isTaskListSet: Boolean
        get() = mTaskList.name.isNotEmpty() && mTaskList.uid.isNotEmpty()

    fun clearTaskList() {
        mTaskList.name = ""
        mTaskList.uid = ""
    }

    var taskList: TaskList
        get() = mTaskList
        set(value) {
            mTaskList = value
        }

    var searchQuery: String
        get() = mSearchQuery
        set(value) {
            mSearchQuery = value
        }

    val isSearchQuerySet: Boolean
        get() = mSearchQuery.isNotEmpty()

    var isInitialized: Boolean
        get() = mIsInitialized
        set(value) {
            mIsInitialized = value
        }

    fun serialize(writer: JsonWriter) {
        with(writer) {
            beginObject()
            name("order")
            value(mOrder.name)
            name("theme")
            value(mTheme.name)
            name("showCompletedItems")
            value(mShowCompletedItems.toString())
            name("smartList")
            value(mSmartList.name)
            name("taskList")
                beginObject()
                name("name")
                value(mTaskList.name)
                name("uid")
                value(mTaskList.uid)
                endObject()
            endObject()
        }
    }

    fun deserialize(reader: JsonReader) {
        with(reader) {
            beginObject()
            while (hasNext()) {
                when (nextName()) {
                    "order" -> {
                        mOrder = Order.valueOf(nextString())
                    }
                    "theme" -> {
                        mTheme = Theme.valueOf(nextString())
                    }
                    "showCompletedItems" -> {
                        mShowCompletedItems = nextString().toBoolean()
                    }
                    "smartList" -> {
                        mSmartList = SmartList.valueOf(nextString())
                    }
                    "taskList" -> {
                        beginObject()
                        while(hasNext()) {
                            when(nextName()) {
                                "name" -> {
                                    mTaskList.name = nextString()
                                }
                                "uid" -> {
                                    mTaskList.uid = nextString()
                                }
                            }
                        }
                        endObject()
                    }
                }
            }
            reader.endObject()
        }
        mIsInitialized = true
    }

    private var mOrder = Order.DUE_DATE
    private var mTheme = Theme.GRAY
    private var mShowCompletedItems: Boolean = false
    private var mSmartList = SmartList.ALL_TASKS
    private var mTaskList = TaskList()
    private var mSearchQuery = String()
    private var mIsInitialized = false
}

open class ThemedActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mUiFile = File(filesDir, "ui.dat")
        mUiState = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(application))[UiState::class.java]

        if(!mUiState.isInitialized) {
            restoreUiState()
        }

        // Set up selected theme
        when(mUiState.theme) {
            UiState.Theme.GRAY -> {
                setTheme(R.style.Theme_EteTasks)
            }
            UiState.Theme.YELLOW -> {
                setTheme(R.style.Theme_EteTasks_Yellow)
            }
            UiState.Theme.BLUE -> {
                setTheme(R.style.Theme_EteTasks_Blue)
            }
        }
    }

    override fun onStop() {
        super.onStop()
        storeUiState()
    }

    protected fun openEncryptedFile(file: File): EncryptedFile {
        val masterKey = MasterKey.Builder(this)
            .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
            .build()
        return EncryptedFile.Builder(
            this,
            file,
            masterKey,
            EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()
    }

    private fun restoreUiState() {
        val uiFile = mUiFile ?: return

        if(uiFile.exists()) {
            val encryptedUiFile = openEncryptedFile(uiFile)
            val parser = Json.createParser(encryptedUiFile.openFileInput())
            val reader = JsonReader(parser)

            mUiState.deserialize(reader)
            reader.close()
        }
        else {
            mUiState.isInitialized = true
        }
    }

    private fun storeUiState() {
        val uiFile = mUiFile ?: return

        uiFile.delete()
        val masterKey = MasterKey.Builder(this)
            .setKeyScheme(MasterKey.KeyScheme.AES256_GCM)
            .build()
        val encryptedUiFile = EncryptedFile.Builder(
            this,
            uiFile,
            masterKey,
            EncryptedFile.FileEncryptionScheme.AES256_GCM_HKDF_4KB
        ).build()
        val generator = Json.createGenerator(encryptedUiFile.openFileOutput())
        val writer = JsonWriter(generator)

        mUiState.serialize(writer)
        writer.close()
    }

    protected var mUiState = UiState()
    private var mUiFile: File? = null
}