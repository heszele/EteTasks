package com.rudolfheszele.etetasks.json

import org.apache.commons.lang3.tuple.MutablePair
import javax.json.stream.JsonParser

class JsonReader(parser: JsonParser) {
    fun hasNext(): Boolean {
        val lastIndex = check(null, null)
        val pair = mEventsQueue[lastIndex]
        return pair.right != pair.left
    }

    fun beginObject() {
        if(mEventsQueue.isEmpty()) {
            if (JsonParser.Event.START_OBJECT != mParser.next()) {
                throw Exception("Invalid object start!")
            }
            mEventsQueue.add(MutablePair(JsonParser.Event.END_OBJECT, mParser.next()))
        }
        else {
            check(null, JsonParser.Event.START_OBJECT)
            mEventsQueue.add(MutablePair(JsonParser.Event.END_OBJECT, mParser.next()))
        }
    }

    fun endObject() {
        val lastIndex = check(JsonParser.Event.END_OBJECT, null)

        mEventsQueue.removeAt(lastIndex)
        if(mEventsQueue.isNotEmpty()) {
            mEventsQueue[lastIndex - 1].right = mParser.next()
        }
    }

    fun beginArray() {
        if(mEventsQueue.isEmpty()) {
            if (JsonParser.Event.START_ARRAY != mParser.next()) {
                throw Exception("Invalid array start!")
            }
            mEventsQueue.add(MutablePair(JsonParser.Event.END_ARRAY, mParser.next()))
        }
        else {
            check(null, JsonParser.Event.START_ARRAY)
            mEventsQueue.add(MutablePair(JsonParser.Event.END_ARRAY, mParser.next()))
        }
    }
    fun endArray() {
        val lastIndex = check(JsonParser.Event.END_ARRAY, null)

        mEventsQueue.removeAt(lastIndex)
        if(mEventsQueue.isNotEmpty()) {
            mEventsQueue[lastIndex - 1].right = mParser.next()
        }
    }

    fun nextName(): String {
        return get(JsonParser.Event.KEY_NAME)
    }

    fun nextString(): String {
        return get(JsonParser.Event.VALUE_STRING)
    }

    fun close() {
        mParser.close()
    }

    private fun check(wantedLeft: JsonParser.Event? = null, wantedRight: JsonParser.Event? = null): Int {
        if(0 == mEventsQueue.size) {
            throw Exception("Invalid request!")
        }

        val lastIndex = mEventsQueue.size - 1
        val pair = mEventsQueue[lastIndex]

        if(null != wantedLeft && pair.left != wantedLeft) {
            throw Exception("Invalid request!")
        }
        if(null != wantedRight && pair.right != wantedRight) {
            throw Exception("Invalid request!")
        }

        return lastIndex
    }

    private fun get(wantedEvent: JsonParser.Event): String {
        val lastIndex = check(null, wantedEvent)
        val result = mParser.string

        mEventsQueue[lastIndex].right = mParser.next()

        return result
    }

    private val mParser = parser
    // Left is the event, marking the end of the current context
    // Right is the next event within the context
    private val mEventsQueue: MutableList<MutablePair<JsonParser.Event, JsonParser.Event>> = mutableListOf()
}