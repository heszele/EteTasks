package com.rudolfheszele.etetasks.json

import javax.json.stream.JsonGenerator

class JsonWriter(generator: JsonGenerator) {
    fun beginObject(): JsonWriter {
        mGenerator.writeStartObject()

        return this
    }
    fun endObject(): JsonWriter {
        mGenerator.writeEnd()

        return this
    }

    fun beginArray(): JsonWriter {
        mGenerator.writeStartArray()

        return this
    }

    fun endArray(): JsonWriter {
        mGenerator.writeEnd()
        return this

    }

    fun name(name: String): JsonWriter {
        mGenerator.writeKey(name)

        return this
    }

    fun value(value: String): JsonWriter {
        mGenerator.write(value)

        return this
    }

    fun close() {
        mGenerator.close()
    }

    private val mGenerator = generator
}
