package com.rudolfheszele.etetasks

// Good source if icons: https://www.svgrepo.com/

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.ProgressBar
import androidx.appcompat.widget.SearchView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton

import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.rudolfheszele.etetasks.etebase.Etebase
import com.rudolfheszele.etetasks.etebase.EtebaseItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import net.fortuna.ical4j.model.Date
import java.io.File
import java.time.Duration
import java.util.concurrent.atomic.AtomicBoolean

const val TAG = "com.rudolfheszele.etetasks.TaskListActivity"

data class ItemHolder(val item: EtebaseItem, val showCollection: Boolean)

internal class ItemsAdapter(context: Context, resource: Int):
    ArrayAdapter<ItemHolder>(context, resource) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var view = convertView
        if(null == view) {
            view = TaskView(parent.context)
        }

        val taskView = view as TaskView? ?: return view
        val itemHolder = getItem(position) ?: return view

        val collection = if(itemHolder.showCollection) itemHolder.item.collection else null
        taskView.setTask(itemHolder.item.task, collection)

        return taskView
    }
}

interface DataSyncListener {
    fun onDataSyncSucceeded()
    fun onDataSyncFailed(exception: Throwable)
}

class TaskListActivity : ThemedActivity(), NavigationView.OnNavigationItemSelectedListener {
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.tasklist_menu, menu)

        mShowCompletedItems = menu.findItem(R.id.show_completed_menu_item)
        mShowCompletedItems?.isChecked = mUiState.isShowCompletedItems

        val searchMenuItem = menu.findItem(R.id.app_bar_search)
        val searchView =  searchMenuItem.actionView as SearchView

        searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if(null != newText) {
                    mUiState.searchQuery = newText
                    setupUi()
                }

                return true
            }

        })
        searchView.queryHint = resources.getString(R.string.search_hint)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(mActionBarDrawerToggle!!.onOptionsItemSelected(item)) {
            return true
        }

        // Handle item selection
        return when (item.itemId) {
            // Sort by... submenus
            R.id.sort_by_due_date_menu_item -> {
                mUiState.order = UiState.Order.DUE_DATE
                setupUi()
                true
            }
            R.id.sort_by_summary_menu_item -> {
                mUiState.order = UiState.Order.SUMMARY
                setupUi()
                true
            }
            // Colours... submenus
            R.id.yellow_colour_menu_item -> {
                mUiState.theme = UiState.Theme.YELLOW
                recreate()
                true
            }
            R.id.gray_colour_menu_item -> {
                mUiState.theme = UiState.Theme.GRAY
                recreate()
                true
            }
            R.id.blue_colour_menu_item -> {
                mUiState.theme = UiState.Theme.BLUE
                recreate()
                true
            }
            R.id.show_completed_menu_item -> {
                item.isChecked = !item.isChecked
                mUiState.isShowCompletedItems = item.isChecked
                setupUi()
                true
            }
            R.id.sync_meny_item -> {
                syncItems()

                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(menuItem: MenuItem): Boolean {
        val collectionIds: Array<Int> = if(mEtebase.hasData()) {
            mEtebase.collections.stream().filter { it.userData is Int }.map { it.userData as Int }.toArray { size -> arrayOfNulls<Int>(size) }
        }
        else {
            arrayOf()
        }

        return when (menuItem.itemId) {
            // Smart Lists submenus
            R.id.all_tasks_menu_item -> {
                mUiState.smartList = UiState.SmartList.ALL_TASKS
                mUiState.clearTaskList()
                setupUi()
                true
            }
            R.id.today_tasks_menu_item -> {
                mUiState.smartList = UiState.SmartList.TODAY
                mUiState.clearTaskList()
                setupUi()
                true
            }
            R.id.next_week_tasks_menu_item -> {
                mUiState.smartList = UiState.SmartList.NEXT_WEEK
                mUiState.clearTaskList()
                setupUi()
                true
            }
            R.id.overdue_tasks_menu_item -> {
                mUiState.smartList = UiState.SmartList.OVERDUE_TASKS
                mUiState.clearTaskList()
                setupUi()
                true
            }
            R.id.completed_tasks_menu_item -> {
                mUiState.smartList = UiState.SmartList.COMPLETED_TASKS
                mUiState.clearTaskList()
                setupUi()
                true
            }
            // Task lists
            in collectionIds -> {
                mEtebase.collections.find {
                    it.userData as Int == menuItem.itemId
                }?.let {
                    mUiState.taskList.uid = it.uid
                    mUiState.taskList.name = it.name
                    mUiState.clearSmartList()
                    setupUi()
                }
                true
            }
            // Others submenus
            R.id.resync_menu_item -> {
                fetchItems()

                true
            }
            R.id.settings_menu_item -> {
                // TODO: Implement this
                true
            }
            R.id.login_menu_item -> {
                startLoginActivity()
                true
            }
            R.id.logout_menu_item -> {
                val context = this

                // Log out asynchronously
                lifecycleScope.launch(Dispatchers.Default) {
                    mEtebase.logout()
                }
                // Delete all stored session/data
                mSessionFile!!.delete()
                mDataFile!!.delete()
                // Remove all items
                mItemsAdapter!!.clear()
                // For whatever reasons it needs to be done async
                lifecycleScope.launch(Dispatchers.Default) {
                    context.runOnUiThread {
                        // Clear navigation drawer
                        mTaskListsMenuItem!!.subMenu.clear()
                        // Set up menu, so the user can log in again
                        mLoginMenuItem!!.isVisible = true
                        mLogoutMenuItem!!.isVisible = false
                        mAddTaskFab!!.visibility = View.GONE
                        mLoginFab!!.visibility = View.VISIBLE
                    }
                }

                true
            }
            else -> false
        }
    }

    fun onLogin(view: View) {
        startLoginActivity()
    }

    fun onAddTask(view: View) {
        // TODO: Implement this
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_task_list)
        setSupportActionBar(findViewById(R.id.taskListsToolbar))

        mDataFile = File(filesDir, "data.dat")
        mSessionFile = File(filesDir, "session.dat")
        mEtebase = ViewModelProvider(this, ViewModelProvider.AndroidViewModelFactory.getInstance(application))[Etebase::class.java]
        mItemsAdapter = ItemsAdapter(this, R.layout.task_details_view)

        // Cache commonly used Views/Menus
        mNavigationView = findViewById(R.id.taskListNavigationView)
        mLoginMenuItem = mNavigationView!!.menu.findItem(R.id.login_menu_item)
        mLogoutMenuItem = mNavigationView!!.menu.findItem(R.id.logout_menu_item)
        mTaskListsMenuItem = mNavigationView!!.menu.findItem(R.id.tasks_lists_menu_item)
        mLoginFab = findViewById(R.id.loginFloatingActionButton)
        mAddTaskFab = findViewById(R.id.addTaskFloatingActionButton)
        mProgressBar = findViewById(R.id.taskListsProgressBar)
        mTasksListView = findViewById(R.id.taskListListView)

        mItemsAdapter!!.setNotifyOnChange(true)

        mTasksListView!!.adapter = mItemsAdapter

        val swipeRefreshLayout = findViewById<SwipeRefreshLayout>(R.id.taskListSwipeRefresh)
        swipeRefreshLayout.setOnRefreshListener {
            syncItems(listener = object: DataSyncListener {
                override fun onDataSyncSucceeded() {
                    swipeRefreshLayout.isRefreshing = false
                }

                override fun onDataSyncFailed(exception: Throwable) {
                    showSnackBar("Failed to sync items: ${exception.message}!")
                    swipeRefreshLayout.isRefreshing = false
                }
            })
        }

        // Set up navigation drawer
        val drawerLayout = findViewById<DrawerLayout>(R.id.taskListDrawerLayout)
        mActionBarDrawerToggle = ActionBarDrawerToggle(this, drawerLayout, R.string.nav_open, R.string.nav_close)

        drawerLayout.addDrawerListener(mActionBarDrawerToggle!!)
        mActionBarDrawerToggle!!.syncState()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        mNavigationView!!.setNavigationItemSelectedListener(this)

        // Check if we need to log in
        if(null != intent && intent.getBooleanExtra(LOGIN_TAG, false)) {
            val success = intent.getBooleanExtra(SUCCESS_TAG, false)

            // Remove the LOGIN_TAG to avoid logging in again again in case the activity is recreated
            intent.removeExtra(LOGIN_TAG)
            if(success) {
                val username = intent.getStringExtra(USERNAME_TAG)
                val password = intent.getStringExtra(PASSWORD_TAG)
                val saveSession = intent.getBooleanExtra(SAVE_SESSION_TAG, false)

                login(username!!, password!!, saveSession)
                return
            }
            else {
                showSnackBar("Failed to log in")
                return
            }
        }

        // Try to restore state
        if(mEtebase.isLoggedIn()) {
            // We are logged in, check if we need to restore data
            if(mEtebase.hasData()) {
                // We have the data, we are done here
                // logoutMenuItem.isVisible = true
                // loginMenuItem.isVisible = false
                setupUi()
            }
            else {
                if(restoreData()) {
                    // We managed to restore data, we are done here
                    // logoutMenuItem.isVisible = true
                    // loginMenuItem.isVisible = false
                    setupUi()
                }
                else {
                    mLogoutMenuItem!!.isVisible = true
                    mLoginMenuItem!!.isVisible = false
                    // We do not have data, try to fetch it
                    fetchItems()
                }

            }
        }
        else {
            if(restoreSession()) {
                // Session restored, try to restore data as well
                if(restoreData()) {
                    setupUi()
                }
                else {
                    mLogoutMenuItem!!.isVisible = true
                    mLoginMenuItem!!.isVisible = false
                    // No data to restore, we need to fetch the items
                    fetchItems()
                }
            }
            else {
                // We could not restore session, so the user needs to log in
                mLogoutMenuItem!!.isVisible = false
                mLoginMenuItem!!.isVisible = true

            }
        }
    }

    override fun onStop() {
        super.onStop()

        // TODO: If the user kills the app, while we are saving data,
        //       restoring it will result in an exception
        storeData()
    }

    private fun storeData() {
        val dataFile = mDataFile ?: return

        dataFile.delete()

        val encryptedDataFile = openEncryptedFile(dataFile)

        with(encryptedDataFile.openFileOutput()) {
            if(!mEtebase.serialize(this).isSuccess) {
                close()
                dataFile.delete()

                return
            }
            close()
        }
    }

    private fun restoreData(): Boolean {
        // TODO: Use progressbar and Snackbars here too
        val dataFile = mDataFile ?: return false

        if(!dataFile.exists()) {
            return false
        }

        val encryptedDataFile = openEncryptedFile(dataFile)

        with(encryptedDataFile.openFileInput()) {
            mEtebase.deserialize(this)
            close()
        }

        return true
    }

    private fun setupUi() {
        mTaskListsMenuItem!!.subMenu.clear()
        // Add all the collections to the navigation drawer
        if(mEtebase.hasData()) {
            for (collection in mEtebase.collections) {
                if (collection.userData !is Int) {
                    collection.userData = View.generateViewId()
                }
                mTaskListsMenuItem!!.subMenu.add(
                    Menu.NONE,
                    collection.userData as Int,
                    Menu.NONE,
                    collection.name
                )
            }
        }

        mShowCompletedItems?.isChecked = mUiState.isShowCompletedItems

        // Add all items to the task list
        mItemsAdapter!!.clear()

        // Collect all the items we should display
        val items = mutableListOf<EtebaseItem>()

        if(mEtebase.hasData()) {
            for (collection in mEtebase.collections) {
                if (mUiState.isTaskListSet && mUiState.taskList.uid != collection.uid) {
                    continue
                }

                items.addAll(collection.items)
            }
        }

        // Handle smart lists
        if(mUiState.isSmartListSet) {
            when(mUiState.smartList) {
                UiState.SmartList.TODAY -> {
                    val today = Date(Date.from(Date().toInstant().plus(Duration.ofDays(1))))

                    items.removeIf {
                        val start = it.task.mainTask.nextStart
                        val end = it.task.mainTask.nextEnd

                        // Remove items whose start date is after today OR
                        // their due date is after today and have no start date
                        (null != start && start.after(today)) ||
                                (null == start && null != end && end.after(today))
                    }
                }
                UiState.SmartList.NEXT_WEEK -> {
                    val nextWeek = Date(Date.from(Date().toInstant().plus(Duration.ofDays(8))))

                    items.removeIf {
                        val start = it.task.mainTask.nextStart
                        val end = it.task.mainTask.nextEnd

                        // Remove items whose start date is after 7 days OR
                        // their due date is after 7 days and have no start date
                        (null != start && start.after(nextWeek)) ||
                                (null == start && null != end && end.after(nextWeek))
                    }
                }
                UiState.SmartList.OVERDUE_TASKS -> {
                    items.removeIf {
                        !it.task.mainTask.isOverdue
                    }
                }
                UiState.SmartList.COMPLETED_TASKS -> {
                    items.removeIf {
                        !it.task.mainTask.isCompleted
                    }
                }
                UiState.SmartList.ALL_TASKS -> {
                    // Do nothing
                }
                UiState.SmartList.NONE -> {
                    throw Exception("Invalid smart list selection!")
                }
            }
        }

        // Handle search
        if(mUiState.isSearchQuerySet) {
            items.removeIf {
                val summary = it.task.mainTask.summary
                val description = it.task.mainTask.description

                (null != summary && !summary.contains(mUiState.searchQuery, ignoreCase = true)) ||
                        (null == summary && null != description && description.contains(mUiState.searchQuery, ignoreCase = true))
            }
        }

        // Order the items
        when(mUiState.order) {
            UiState.Order.DUE_DATE -> items.sortBy { it.task.mainTask.nextEnd }
            UiState.Order.SUMMARY -> items.sortBy { it.task.mainTask.summary }
        }

        for(item in items) {
            if(!mUiState.isShowCompletedItems && item.task.mainTask.isCompleted) {
                continue
            }

            // Only show the collection the task belongs to, when we show a smart task list
            // otherwise all the tasks are from the same list
            mItemsAdapter!!.add(ItemHolder(item, mUiState.isSmartListSet))
        }

        // Hide/show Views based on state
        if(!mIsSyncJobRunning.get()) {
            mProgressBar!!.visibility = View.GONE
        }
        if(mEtebase.isLoggedIn()) {
            mLoginMenuItem!!.isVisible = false
            mLogoutMenuItem!!.isVisible = true
            mLoginFab!!.visibility = View.GONE
            mAddTaskFab!!.visibility = View.VISIBLE
        }
        else {
            mLoginMenuItem!!.isVisible = true
            mLogoutMenuItem!!.isVisible = false
            mLoginFab!!.visibility = View.VISIBLE
            mAddTaskFab!!.visibility = View.GONE
        }

        // Set title
        title = if(mUiState.isTaskListSet) {
            mUiState.taskList.name
        } else {
            when(mUiState.smartList) {
                UiState.SmartList.ALL_TASKS -> {
                    resources.getString(R.string.all_tasks_menu_item)
                }
                UiState.SmartList.TODAY -> {
                    resources.getString(R.string.today_tasks_menu_item)
                }
                UiState.SmartList.NEXT_WEEK -> {
                    resources.getString(R.string.next_week_tasks_menu_item)
                }
                UiState.SmartList.OVERDUE_TASKS -> {
                    resources.getString(R.string.overdue_tasks_menu_item)
                }
                UiState.SmartList.COMPLETED_TASKS -> {
                    resources.getString(R.string.completed_tasks_menu_item)
                }
                UiState.SmartList.NONE -> {
                    throw Exception("Invalid smart list selection!")
                }
            }
        }
    }

    private fun restoreSession(): Boolean {
        val sessionFile = mSessionFile ?: return false

        if(!sessionFile.exists()) {
            return false
        }

        val encryptedSessionFile = openEncryptedFile(sessionFile)

        return mEtebase.restoreSession(encryptedSessionFile.openFileInput()).isSuccess
    }

    private fun storeSession(): Result<Boolean> {
        val sessionFile = mSessionFile ?: return Result.failure(Exception("No session file to save to"))

        sessionFile.delete()

        val encryptedSessionFile = openEncryptedFile(sessionFile)

        return mEtebase.storeSession(encryptedSessionFile.openFileOutput())
    }

    private fun login(username: String, password: String, saveSession: Boolean): Boolean {
        val context = this

        showSnackBar("Logging in...")
        // Remove every login/logout related UI
        mLoginMenuItem!!.isVisible = false
        mLogoutMenuItem!!.isVisible = false
        mLoginFab!!.visibility = View.GONE
        lifecycleScope.launch(Dispatchers.Default) {
            val loginResult = mEtebase.login(username, password)

            if(!loginResult.isSuccess) {
                context.runOnUiThread {
                    context.onLoginFailed(loginResult.exceptionOrNull()!!.message!!)
                }
                return@launch
            }

            // TODO: Save session should not be here, but since we are doing an async operation
            //       this is the simplest way to do for now
            if(saveSession) {
                val storeSessionResult = storeSession()

                if(!storeSessionResult.isSuccess) {
                    context.runOnUiThread {
                        context.onStoreSessionFailed(storeSessionResult.exceptionOrNull()!!.message!!)
                    }
                    return@launch
                }
                else {
                    context.runOnUiThread {
                        context.onStoreSessionSucceeded()
                    }
                }
            }

            context.runOnUiThread {
                context.onLoginSucceeded()
            }
        }

        return true
    }

    private fun onLoginSucceeded() {
        fetchItems(snackBar = "Successful login, fetching items...")
    }

    private fun onLoginFailed(message: String) {
        showSnackBar("Failed to log in: $message")
        // Login is possible again
        mLoginMenuItem!!.isVisible = true
        mLogoutMenuItem!!.isVisible = false
        mLoginFab!!.visibility = View.VISIBLE
    }

    private fun onStoreSessionSucceeded() {
        // Nothing to do yet
    }

    private fun onStoreSessionFailed(message: String) {
        showSnackBar("Failed to store session: $message")
    }

    private fun fetchItems(listener: DataSyncListener? = null, snackBar: String = "Fetching items...") {
        mItemsAdapter!!.clear()
        mProgressBar!!.visibility = View.VISIBLE
        syncData(true, listener)
        showSnackBar(snackBar)
    }

    private fun syncItems(listener: DataSyncListener? = null, snackBar: String = "Syncing items...") {
        mProgressBar!!.visibility = View.VISIBLE
        syncData(false, listener)
        showSnackBar(snackBar, BaseTransientBottomBar.LENGTH_SHORT)
    }

    private fun showSnackBar(message: String, duration: Int = BaseTransientBottomBar.LENGTH_LONG) {
        Snackbar.make(mTasksListView!!, message, duration).show()
    }

    private fun startLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)

        // Login will "call back" for us, and we do not want the user to be able to
        // come back to this instance of the Activity
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(intent)
    }

    private fun syncData(reSync: Boolean,
                         listener: DataSyncListener? = null) {
        val context = this

        // TODO: Since this async job is running in the lifecycleScope
        //       when the activity is paused the thread is killed
        //       That results in an empty list, since the views could not be added to the list
        // TODO: It is extremely slow! I am not sure if it is a real issue, or something with
        //       the Android Studio, but parsing some items can take up to half a minute
        //       Would be great to see in the profiler which method takes the time
        //       but it really looks like that the calender parsing is really slow
        //       NOTE: Looks like it is only slow during debugging
        lifecycleScope.launch(Dispatchers.Default) {
            mIsSyncJobRunning.set(true)
            val result = if(reSync) mEtebase.reSync() else mEtebase.sync()
            mIsSyncJobRunning.set(false)

            if(result.isFailure) {
                if(null != listener) {
                    context.runOnUiThread {
                        listener.onDataSyncFailed(result.exceptionOrNull()!!)
                    }
                }

                return@launch
            }

            context.runOnUiThread {
                setupUi()
            }

            storeData()

            if(null != listener) {
                context.runOnUiThread {
                    listener.onDataSyncSucceeded()
                }
            }
        }
    }

    private var mActionBarDrawerToggle: ActionBarDrawerToggle? = null
    private var mEtebase = Etebase()
    private var mDataFile: File? = null
    private var mSessionFile: File? = null
    private var mItemsAdapter: ItemsAdapter? = null
    private val mIsSyncJobRunning = AtomicBoolean(false)
    private var mShowCompletedItems: MenuItem? = null
    private var mNavigationView: NavigationView? = null
    private var mLoginMenuItem: MenuItem? = null
    private var mLogoutMenuItem: MenuItem? = null
    private var mTaskListsMenuItem: MenuItem? = null
    private var mLoginFab: FloatingActionButton? = null
    private var mAddTaskFab: FloatingActionButton? = null
    private var mProgressBar: ProgressBar? = null
    private var mTasksListView: ListView? = null
}