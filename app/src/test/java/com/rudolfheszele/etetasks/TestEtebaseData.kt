package com.rudolfheszele.etetasks

import com.rudolfheszele.etetasks.json.JsonReader
import org.junit.Assert.*
import org.junit.Test
import java.io.ByteArrayInputStream
import javax.json.*

class TestEtebaseData {
    @Test
    fun test_EtebaseItemCanBeDeserializedFromJson() {
        val ical = """BEGIN:VCALENDAR
VERSION:2.0
PRODID:+//IDN tasks.org//android-111007//EN
BEGIN:VTODO
DTSTAMP:20210830T122846Z
UID:4358187658129599488
CREATED:20210830T122649Z
LAST-MODIFIED:20210830T122729Z
SUMMARY:Test summary
PRIORITY:9
DUE;TZID=Europe/Stockholm:20210831T200001
DTSTART;TZID=Europe/Stockholm:20210831T090000
END:VTODO
BEGIN:VTIMEZONE
TZID:Asia/Tokyo
BEGIN:STANDARD
TZNAME:JST
TZOFFSETFROM:+0900
TZOFFSETTO:+0900
END:STANDARD
END:VTIMEZONE
END:VCALENDAR
"""
        val etebaseItemJson = """{
"uid": "TestUid",
"content": "${ical.replace("\n", "\\n").replace("\r", "")}"
}
"""




//        val etebaseDataJson = """{
//                "stoken": "dataStoken",
//                "collections": [
//                    {
//                        "stoken": "collection1Stoken",
//                        "uid": "collection1Uid",
//                        "name": "collection1Name",
//                        "items": [
//                            {
//                                "uid": "Collection1Item1Uid",
//                                "content": "${ical.replace("\n", "\\n").replace("\r", "")}"
//                            },
//                            {
//                                "uid": "Collection1Item2Uid",
//                                "content": "${ical.replace("\n", "\\n").replace("\r", "")}"
//                            }
//                        ]
//                    },
//                    {
//                        "stoken": "collection2Stoken",
//                        "uid": "collection2Uid",
//                        "name": "collection2Name",
//                        "items": [
//                            {
//                                "uid": "Collection2Item1Uid",
//                                "content": "${ical.replace("\n", "\\n").replace("\r", "")}"
//                            }
//                        ]
//                    }
//                ]
//            }
//        """.trimIndent()

        val reader = JsonReader(Json.createParser(ByteArrayInputStream(etebaseItemJson.toByteArray(Charsets.UTF_8))))
        val etebaseItem = EtebaseItem(reader)

        assertEquals(etebaseItem.uid, "TestUid")
        assertEquals(etebaseItem.calendar.toString().replace("\r", ""), ical)
    }
}