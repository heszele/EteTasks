package com.rudolfheszele.etetasks

import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.model.Component
import net.fortuna.ical4j.model.Date
import net.fortuna.ical4j.model.DateTime
import net.fortuna.ical4j.model.TimeZone
import net.fortuna.ical4j.model.component.VTimeZone
import org.junit.Test
import org.junit.Assert.*
import java.io.StringReader

class TestTaskDate {
    @Test
    fun test_TaskDateCanBeCreatedWithNull() {
        val taskDate = TaskDate(null)

        assertNull(taskDate.date)
        assertNull(taskDate.utcDateTime)
    }

    @Test
    fun test_TaskDateCanBeCreatedWithDate() {
        val taskDate = TaskDate(Date())

        assertNotNull(taskDate.date)
        assertNull(taskDate.utcDateTime)
    }

    @Test
    fun test_TaskDateCanBeCreatedWithDateTime() {
        val taskDate = TaskDate(DateTime())

        assertNotNull(taskDate.date)
        assertNotNull(taskDate.utcDateTime)
    }

    @Test
    fun test_TaskDateCanBeUsedToCompareTimeDatesWithDifferentTimezones() {
        val timeZoneAiCal = """BEGIN:VCALENDAR
BEGIN:VTIMEZONE
TZID:Europe/Stockholm
BEGIN:STANDARD
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10
TZNAME:CET
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3
TZNAME:CEST
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
END:DAYLIGHT
END:VTIMEZONE
END:VCALENDAR"""
        val timeZoneA = getTimeZone(timeZoneAiCal)

        val timeZoneBiCal = """BEGIN:VCALENDAR
BEGIN:VTIMEZONE
TZID:Asia/Tokyo
BEGIN:STANDARD
TZNAME:JST
TZOFFSETFROM:+0900
TZOFFSETTO:+0900
END:STANDARD
END:VTIMEZONE
END:VCALENDAR"""
        val timeZoneB = getTimeZone(timeZoneBiCal)

        // During winter timeZoneA is only UTC + 1
        val taskDateAWinter = TaskDate(DateTime("20210101T090000", timeZoneA))
        val taskDateBWinter = TaskDate(DateTime("20210101T170000", timeZoneB))

        assertEquals(taskDateAWinter, taskDateBWinter)

        // But during summer it is UTC + 2
        val taskDateASummer = TaskDate(DateTime("20210801T100000", timeZoneA))
        val taskDateBSummer = TaskDate(DateTime("20210801T170000", timeZoneB))

        assertEquals(taskDateASummer, taskDateBSummer)

        val taskDateAOverNight = TaskDate(DateTime("20210801T200000", timeZoneA))
        val taskDateBOverNight = TaskDate(DateTime("20210802T030000", timeZoneB))

        assertEquals(taskDateAOverNight, taskDateBOverNight)
    }

    private fun getTimeZone(ical: String): TimeZone {
        val calendar = CalendarBuilder().build(StringReader(ical))
        val vTimeZone = calendar.getComponent(Component.VTIMEZONE) as VTimeZone

        assertNotNull(vTimeZone)

        return TimeZone(vTimeZone)
    }
}