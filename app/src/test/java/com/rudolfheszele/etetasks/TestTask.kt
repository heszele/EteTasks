package com.rudolfheszele.etetasks

import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.model.DateTime

import org.junit.Assert.*
import org.junit.Test

import java.io.StringReader
import java.time.Duration
import java.util.Date

class TestTask {
    @Test
    fun test_TaskCanBeCreatedFromACalendar() {
        val simpleTask = getTask(getSimpleCalendarICal())

        assertNotNull(simpleTask.mainTask)
        assertEquals(0, simpleTask.subTasks.size)

        val recurringTask = getTask(getRecurringCalendarICal())

        assertNotNull(recurringTask.mainTask)
        assertEquals(0, recurringTask.subTasks.size)

        val subTasks = getTask(getRecurringTaskWithSubtasksICal())
        assertNotNull(subTasks.mainTask)
        assertEquals(1, subTasks.subTasks.size)
    }

    @Test
    fun test_TaskCanParseSimpleVTodoComponent() {
        val now = DateTime()
        val duration = Duration.ofHours(1)
        val start = DateTime(Date.from(now.toInstant().minus(duration)))
        val end = DateTime(Date.from(now.toInstant().plus(duration)))
        val simpleTask = getTask(getSimpleCalendarICal(start, end))
        val mainTask = simpleTask.mainTask

        assertEquals(mainTask.uid, "4358187658129599488")
        assertEquals(mainTask.summary, "Test Summary")
        assertEquals(mainTask.description, "Test Description")
        assertEquals(mainTask.lastModified, DateTime("20210830T122729Z"))
        assertFalse(mainTask.isRecurring)
        assertFalse(mainTask.isCompleted)
        assertEquals(mainTask.start, start)
        assertEquals(mainTask.end, end)
        assertEquals(mainTask.duration, duration.plus(duration))
        // Since it is a non-recurring task, start/end should be equal to nextStart/nextEnd
        assertEquals(mainTask.start, mainTask.nextStart)
        assertEquals(mainTask.end, mainTask.nextEnd)
        assertTrue(mainTask.isOngoing)
        assertFalse(mainTask.isOverdue)
    }

    // TODO: Add additional tests

    private fun getSimpleCalendarICal(start: DateTime = DateTime(), end: DateTime = DateTime()): String {
        return """BEGIN:VCALENDAR
VERSION:2.0
PRODID:+//IDN tasks.org//android-111007//EN
BEGIN:VTODO
DTSTAMP:20210830T122846Z
UID:4358187658129599488
CREATED:20210830T122649Z
LAST-MODIFIED:20210830T122729Z
SUMMARY:Test Summary
PRIORITY:9
X-APPLE-SORT-ORDER:648937628
DUE;TZID=Europe/Stockholm:${end}
DTSTART;TZID=Europe/Stockholm:${start}
DESCRIPTION:Test Description
END:VTODO
BEGIN:VTIMEZONE
TZID:Europe/Stockholm
LAST-MODIFIED:20201010T011803Z
BEGIN:DAYLIGHT
TZNAME:CEST
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
DTSTART:19810329T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
END:DAYLIGHT
BEGIN:STANDARD
TZNAME:CET
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
DTSTART:19961027T030000
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
END:STANDARD
END:VTIMEZONE
END:VCALENDAR"""
    }

    private fun getRecurringCalendarICal(start: DateTime = DateTime(), end: DateTime = DateTime()): String {
        return """BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//Mozilla.org/NONSGML Mozilla Calendar V1.1//EN
BEGIN:VTIMEZONE
TZID:Europe/Stockholm
BEGIN:STANDARD
DTSTART:19701025T030000
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10
TZNAME:CET
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:19700329T020000
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3
TZNAME:CEST
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
END:DAYLIGHT
END:VTIMEZONE
BEGIN:VTODO
CREATED:20210726T083813Z
DTSTAMP:20210827T160318Z
DTSTART;TZID=Europe/Stockholm:${start}
DUE;TZID=Europe/Stockholm:${end}
EXDATE:20210821T070000Z
LAST-MODIFIED:20210827T160318Z
PRIORITY:9
RRULE:FREQ=WEEKLY;INTERVAL=2;BYDAY=SA
SEQUENCE:1
SUMMARY:Test Summary
UID:911584955016032936
X-MOZ-GENERATION:2
END:VTODO
END:VCALENDAR"""
    }

    private fun getRecurringTaskWithSubtasksICal(start: DateTime = DateTime(), end: DateTime = DateTime()): String {
        return """BEGIN:VCALENDAR
VERSION:2.0
PRODID:-//MailClient.VObject/8.2.1473.0
BEGIN:VTIMEZONE
TZID:W. Europe Standard Time
BEGIN:STANDARD
DTSTART:00010101T030000
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10
TZNAME:STANDARD
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:00010101T020000
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3
TZNAME:DAYLIGHT
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
END:DAYLIGHT
END:VTIMEZONE
BEGIN:VTIMEZONE
TZID:Europe/Stockholm
LAST-MODIFIED:20201010T011803Z
BEGIN:STANDARD
DTSTART:19961027T030000
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=10
TZNAME:CET
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
END:STANDARD
BEGIN:DAYLIGHT
DTSTART:19810329T020000
RRULE:FREQ=YEARLY;BYDAY=-1SU;BYMONTH=3
TZNAME:CEST
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
END:DAYLIGHT
END:VTIMEZONE
BEGIN:VTODO
CREATED:20210726T082524Z
DTSTAMP:20210807T101156Z
DTSTART;TZID=Europe/Stockholm:${start}
DUE;TZID=Europe/Stockholm:${end}
LAST-MODIFIED:20210807T101156Z
PERCENT-COMPLETE:0
PRIORITY:9
RRULE:FREQ=WEEKLY;INTERVAL=2;BYDAY=SA
SUMMARY:Test Summary
UID:992064443268155320
X-APPLE-SORT-ORDER:648978963
END:VTODO
BEGIN:VTODO
COMPLETED:20210807T101156Z
CREATED:20210807T101156Z
DTSTAMP:20210807T101156Z
DTSTART;TZID=W. Europe Standard Time:${start}
DUE;TZID=W. Europe Standard Time:${end}
LAST-MODIFIED:20210807T101156Z
PERCENT-COMPLETE:100
RECURRENCE-ID;TZID=W. Europe Standard Time:20210807T090000
STATUS:COMPLETED
SUMMARY:Test Summary
UID:992064443268155320
END:VTODO
END:VCALENDAR"""
    }

    private fun getTask(ical: String): Task {
        val calendar = CalendarBuilder().build(StringReader(ical))

        return Task(calendar)
    }
}