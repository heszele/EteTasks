package com.rudolfheszele.etetasks

import net.fortuna.ical4j.data.CalendarBuilder
import net.fortuna.ical4j.model.Component
import net.fortuna.ical4j.model.Date
import net.fortuna.ical4j.model.DateTime
import net.fortuna.ical4j.model.TimeZone
import net.fortuna.ical4j.model.component.VTimeZone
import net.fortuna.ical4j.util.TimeZones
import org.junit.Test

import org.junit.Assert.*
import java.io.StringReader
import java.text.SimpleDateFormat
import java.time.Duration
import java.time.temporal.TemporalAmount

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    private fun getICalendar(summary: String, start: DateTime, duration: TemporalAmount): String {
        val end = DateTime(java.util.Date.from(start.toInstant().plus(duration)), start.timeZone)
        return """BEGIN:VCALENDAR
VERSION:2.0
PRODID:+//IDN tasks.org//android-111007//EN
BEGIN:VTODO
DTSTAMP:20210830T122846Z
UID:4358187658129599488
CREATED:20210830T122649Z
LAST-MODIFIED:20210830T122729Z
SUMMARY:${summary}
PRIORITY:9
X-APPLE-SORT-ORDER:648937628
DUE;TZID=Europe/Stockholm:${end}
DTSTART;TZID=Europe/Stockholm:${start}
END:VTODO
BEGIN:VTIMEZONE
TZID:Europe/Stockholm
LAST-MODIFIED:20201010T011803Z
BEGIN:DAYLIGHT
TZNAME:CEST
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
DTSTART:19810329T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
END:DAYLIGHT
BEGIN:STANDARD
TZNAME:CET
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
DTSTART:19961027T030000
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
END:STANDARD
END:VTIMEZONE
END:VCALENDAR"""
    }
    @Test
    fun test_SimpleTask() {
        val summary = "Test summary"
        val now = DateTime()
        val duration = java.time.Duration.ofHours(10)
        val iCal2 = getICalendar(summary, now, duration)

        //java.time.ZoneId.
        // val timeZones = java.util.TimeZone.getAvailableIDs()
        //val timeZone = java.util.TimeZone.getDefault()
        //val vTimeZone = VTimeZone()
        //val timeZone = TimeZone()
        //val timezone = java.util.TimeZones
        val ical = """BEGIN:VCALENDAR
VERSION:2.0
PRODID:+//IDN tasks.org//android-111007//EN
BEGIN:VTODO
DTSTAMP:20210830T122846Z
UID:4358187658129599488
CREATED:20210830T122649Z
LAST-MODIFIED:20210830T122729Z
SUMMARY:${summary}
PRIORITY:9
X-APPLE-SORT-ORDER:648937628
DUE;TZID=Europe/Stockholm:20210831T200001
DTSTART;TZID=Europe/Stockholm:20210831T090000
END:VTODO
BEGIN:VTIMEZONE
TZID:Europe/Stockholm
LAST-MODIFIED:20201010T011803Z
BEGIN:DAYLIGHT
TZNAME:CEST
TZOFFSETFROM:+0100
TZOFFSETTO:+0200
DTSTART:19810329T020000
RRULE:FREQ=YEARLY;BYMONTH=3;BYDAY=-1SU
END:DAYLIGHT
BEGIN:STANDARD
TZNAME:CET
TZOFFSETFROM:+0200
TZOFFSETTO:+0100
DTSTART:19961027T030000
RRULE:FREQ=YEARLY;BYMONTH=10;BYDAY=-1SU
END:STANDARD
END:VTIMEZONE
END:VCALENDAR"""
        val calendar = CalendarBuilder().build(StringReader(ical))
        val task = Task(calendar)
        val timeZone = TimeZone(calendar.getComponents<VTimeZone>(Component.VTIMEZONE).first())

        assertNotNull(timeZone)
        assertEquals(task.subTasks.size, 0)
        assertEquals(task.mainTask.summary, summary)

        val end = task.mainTask.end as DateTime
        assertNotNull(end)
        val start = task.mainTask.start as DateTime
        assertNotNull(start)

        assertEquals(end, DateTime("20210831T200001", timeZone))
        assertEquals(start, DateTime("20210831T090000", timeZone))
        assertEquals(end.timeZone, timeZone)
        assertEquals(start.timeZone, timeZone)

        // Since this is a non-recurring task, end/start should be the same as nextEnd/nextStart
        assertEquals(end, task.mainTask.nextEnd)
        assertEquals(start, task.mainTask.nextStart)
    }
}