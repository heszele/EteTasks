package com.rudolfheszele.etetasks.json

import org.junit.Test
import org.junit.Assert.*
import java.io.StringReader
import javax.json.Json

class TestJsonReader {
    @Test
    fun test_JsonReaderCanParseSimpleJson() {
        val json = """{"name":"value"}"""
        val parser = Json.createParser(StringReader(json))
        val reader = JsonReader(parser)
        var value = String()

        with(reader) {
            beginObject()
            while(hasNext()) {
                when(nextName()) {
                    "name" -> {
                        value = nextString()
                    }
                }
            }
            endObject()
            close()
        }
        assertEquals(value, "value")
    }

    @Test
    fun test_JsonReaderCanParseNestedJson() {
        val json = """{"name1":"value1","array":[{"name2":"value2"},{"name2":"value3"}]}"""
        val parser = Json.createParser(StringReader(json))
        val reader = JsonReader(parser)
        var value = String()
        val values = mutableListOf<String>()

        with(reader) {
            beginObject()
            while(hasNext()) {
                when(nextName()) {
                    "name1" -> {
                        value = nextString()
                    }
                    "array" -> {
                        beginArray()
                        while(hasNext()) {
                            beginObject()
                            while (hasNext()) {
                                when (nextName()) {
                                    "name2" -> {
                                        values.add(nextString())
                                    }
                                }

                            }
                            endObject()
                        }
                        endArray()
                    }
                }
            }
            endObject()
            close()
        }
        assertEquals(value, "value1")
        assertEquals(values.size, 2)
        assertEquals(values[0], "value2")
        assertEquals(values[1], "value3")
    }
}