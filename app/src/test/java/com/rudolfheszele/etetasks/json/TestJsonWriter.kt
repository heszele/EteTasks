package com.rudolfheszele.etetasks.json

import org.junit.Test
import org.junit.Assert.*
import java.io.ByteArrayOutputStream
import javax.json.Json

class TestJsonWriter {
    @Test
    fun test_JsonWriterCanGenerateSimpleJson() {
        val output = ByteArrayOutputStream()
        val generator = Json.createGenerator(output)
        val writer = JsonWriter(generator)

        with(writer) {
            beginObject()
            name("name")
            value("value")
            endObject()
            close()
        }

        assertEquals("""{"name":"value"}""", output.toString(Charsets.UTF_8.name()))
    }

    @Test
    fun test_JsonWriterCanGenerateNestedJson() {
        val output = ByteArrayOutputStream()
        val generator = Json.createGenerator(output)
        val writer = JsonWriter(generator)

        with(writer) {
            beginObject()
            name("name1")
            value("value1")
            name("array")
            beginArray()
            beginObject()
            name("name2")
            value("value2")
            endObject()
            beginObject()
            name("name3")
            value("value3")
            endObject()
            endArray()
            endObject()
            close()
        }
        assertEquals("""{"name1":"value1","array":[{"name2":"value2"},{"name3":"value3"}]}""", output.toString(Charsets.UTF_8.name()))
    }
}